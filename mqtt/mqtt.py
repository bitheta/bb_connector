import paho.mqtt.client as mqtt
import ssl
import logging

class MqttClient():
    def __init__(self, profile = {}):
        self.profile = profile
        client = mqtt.Client()
        if profile['secure']:
            client.tls_set(cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1_2)
            client.tls_insecure_set(profile['skipSslVerify'])
        logging.debug(f'username: {self.profile["username"]}, password: {self.profile["password"]}')
        client.username_pw_set(username=profile['username'], password=profile['password'])
        self.client = client

    def connect(self):
        logging.debug(f'host: {self.profile["hostname"]}, port: {self.profile["port"]}')
        self.client.connect(host=self.profile['hostname'], port=self.profile['port'])
        self.client.loop_start()

    def close(self):
        self.client.loop_stop(force=False)

    def publish(self, topic, payload):
        ret = self.client.publish(topic=topic, payload=payload, qos=1, retain=False)
        if ret[0] != 0:
            logging.debug(mqtt.connack_string(ret[0]))
            logging.error(f'Unable to publish message. topic: {topic}, payload: {payload} ')
