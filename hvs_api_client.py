
import logging
import os
import mimetypes
from urllib.parse import urljoin, urlparse
import requests

try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin


METHODS = ['GET', 'POST', 'HEAD', 'TRACE', 'PUT', 'DELETE', 'OPTIONS', 'CONNECT']
BASE_PATH = os.path.dirname(__file__)
ENTITY_PAYLOAD_TEMP = os.path.join(BASE_PATH, 'entity_payload.json')
ENTITY_TYPE_PAYLOAD_TEMP = os.path.join(BASE_PATH, 'entity_type_payload.json')

class UnSupportMethodException(Exception):
    """throw exception when method doesn't support"""
    pass


class HTTPClient(object):
    def __init__(self, url, method='GET', headers=None, cookies=None):
        self.url = url
        self.session = requests.session()
        self.method = method.upper()
        if self.method not in METHODS:
            raise UnSupportMethodException('Unsupported Method!'.format(self.method))

        self.set_headers(headers)
        self.set_cookies(cookies)

    def set_headers(self, headers):
        if headers:
            self.session.headers.update(headers)

    def set_cookies(self, cookies):
        if cookies:
            self.session.cookies.update(cookies)

    def send(self, params=None, data=None, **kwargs):
        response = self.session.request(method=self.method, url=self.url, params=params, json=data, **kwargs)
        response.encoding = 'utf-8'
        return response


class HVSAPIClient:
    def __init__(self, url):
        self.base_url = url
        self.entity_payload = JsonReader(ENTITY_PAYLOAD_TEMP).data
        self.entity_type_payload = JsonReader(ENTITY_TYPE_PAYLOAD_TEMP).data
        self.headers = ""

    def get_url(self, url):
        return urljoin(self.base_url, url)

    def login(self, username, password, domain):
        url = self.get_url('authenticate')
        data = {'username': username, 'password': password, "domain": domain}
        client = HTTPClient(url=url, method='POST')
        response = client.send(data=data)
        return response

    def get_authentication_token(self, response):
        response_data = response.json()
        token = response_data["data"]["access_token"]
        authenticate_header = {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        }
        self.headers = authenticate_header
        return authenticate_header

    def create_entity_type_for_test(self, entity_type, location):
        url = self.get_url("/management/entitytypes")
        self.entity_type_payload["entity_type"]["appearance"]["color"] = entity_type["appearance"]["color"]
        self.entity_type_payload["entity_type"]["appearance"]["background_color"] = entity_type["appearance"]["color"]
        self.entity_type_payload["entity_type"]["appearance"]["domain_panel_location"] = location
        self.entity_type_payload["entity_type"]["name"] = entity_type["name"]
        client = HTTPClient(url=url, method='POST', headers=self.headers)
        result = client.send(data=self.entity_type_payload).json()
        return result['data']['id']

    def create_entity_type(self):
        url = self.get_url("/management/entitytypes")
        client = HTTPClient(url=url, method='POST', headers=self.headers)
        result = client.send(data=self.entity_type_payload).json()
        return result['data']['id']

    # def create_entity_with_random_location(self, entity, entity_type_id, entity_config):
    #     url = self.get_url("/entitytypes/" + entity_type_id + "/entities")
    #     self.entity_payload["name"] = entity["name"]
    #     self.entity_payload["description"] = entity["description"]
    #     location_time_generator_object = LocationTimeGenerator()
    #     self.entity_payload["location"]["coordinates"] = location_time_generator_object.generate_random_coords(
    #         entity_config["entity_config"]["ylon0"], entity_config["entity_config"]["xlat0"],
    #         entity_config["entity_config"]["ylon1"], entity_config["entity_config"]["xlat1"])
    #     self.entity_payload["timestamp"] = location_time_generator_object.randomDate(
    #         entity_config["entity_config"]["date_from"],
    #         entity_config["entity_config"]["date_to"],
    #         random.random())
    #     self.entity_payload["attributes"]["caseID"]["value"] = "12311"
    #     self.entity_payload["attributes"]["reportedBy"]["value"] = "Tom"
    #     self.entity_payload["attributes"]["Context"]["value"] = ""
    #     client = HTTPClient(url=url, method='POST', headers=self.headers)
    #     client.send(data=self.entity_payload)

    def create_entity(self, entity, entity_type_id):
        url = self.get_url("/entitytypes/" + entity_type_id + "/entities")
        client = HTTPClient(url=url, method='POST', headers=self.headers)
        result = client.send(data=entity).json()
        return result['data']['id']

    def delete_entity(self, entity_id, entity_type_id):
        url = self.get_url("/entitytypes/" + entity_type_id + "/entities/" + entity_id)
        client = HTTPClient(url=url, method='DELETE', headers=self.headers)
        response = client.send()
        return response




import json
import os

class JsonReader:
    def __init__(self, jsonf):
        if os.path.exists(jsonf):
            self.jsonf = jsonf
        else:
            raise FileNotFoundError('file not found!')
        self._data = None

    @property
    def data(self):
        if not self._data:
            with open(self.jsonf, 'r') as f:
                self._data = json.load(f)
            return self._data