from Crypto.Cipher import XOR  # PyCrypto
import base64

dk = '_mmYSu63rK8y!_'

def encrypt(plaintext, key=dk):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))

def decrypt(ciphertext, key=dk):
    cipher = XOR.new(key)
    return cipher.decrypt(base64.b64decode(ciphertext))
