import sys

def print_logo(path):
    with open(path) as f:
        for line in f:
            sys.stdout.write(line)
        sys.stdout.write('\n')
        