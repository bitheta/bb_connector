import os
import json
import logging.config

conf = {}
print(os.getcwd())
if os.path.exists("config.json"):
    with open('config.json', 'r') as config_file:
        config_raw = config_file.read()
        conf = json.loads(config_raw)

logging.config.dictConfig(conf['log'])