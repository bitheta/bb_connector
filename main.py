import time
import logging
import datetime
import json
import os
#import argparse
import websocket
import pprint
import random
from lib import app_config as app
from lib import terminal_utils
from hvs_api_client import HVSAPIClient
from mqtt.mqtt import MqttClient
from handwashing_event import HandwashingEvent
from sys import platform
from hvs_api_utils import HVSApiUtils

pp = pprint.PrettyPrinter(indent=4)
handwash_event = None
mqtt_topic = None
mqtt_event_topic = None
mqttc = None

def on_ws_message(ws, message):
    global handwash_event
    ws.send("[triggers]")
    jmsg = json.loads(message)

    payload = json.dumps(jmsg)
    mqttc.publish(mqtt_topic, payload)

    if 'zones' in jmsg:
        for zone in jmsg['zones']:
            is_triggered = 'triggered' in zone and zone['triggered']
            if zone['name'] == 'Occupied':
                if is_triggered:
                    if not handwash_event:
                        handwash_event = HandwashingEvent()
                else:
                    # finalize event e.g. calculate hand washing duration
                    if handwash_event:
                        handwash_event.stop()
                        # send message to HVS API
                        logging.debug('send message to HVS API')
                        logging.debug(pp.pformat(handwash_event.to_json()))
                        mqttc.publish(mqtt_event_topic, handwash_event.to_json())
                        # post_event(handwash_event)
                        handwash_event = None
            if zone['name'] == 'Soap' and is_triggered:
                handwash_event.soap_used = 1
            if zone['name'] == 'Washing':
                if is_triggered and handwash_event.in_hand_washing_time is None:
                    handwash_event.start_handwashing()
                else:
                    if handwash_event and handwash_event.in_hand_washing_time is not None:
                        handwash_event.stop_handwashing()
            if zone['name'] == 'Towel' and is_triggered:
                handwash_event.towel_used = 1
def on_ws_error(ws, error):
    logging.error(error)
def on_ws_close(ws):
    logging.info("websocket closed")
def on_ws_open(ws):
    ws.send("[triggers]")


if __name__ == "__main__":
    if not platform == "win32":
        terminal_utils.print_logo("logo.txt")
    try:
        logging.debug("Starting BB")
        if app.conf['mock_data']:
            # we simulate data and we post to HVS REST API
            while True:
                try:
                    entity_id = HVSApiUtils.post_mock_event()

                except Exception as ex:
                    logging.error(ex)
                finally:
                    time.sleep(5)
        else:
            # We gather data from sensor fusion through WS and then publish on MQTT to Lumada Edge and HVS REST API

            # for some reason the pod could not resolve hostname `mqtt` on init
            # wait for hostname to be ready
            time.sleep(2)
            # Get mqtt client
            mqtt_profile = app.conf['mqtt']
            logging.debug(mqtt_profile)
            mqtt_topic = mqtt_profile['topic'] or 'bb-connector-default-topic'
            mqtt_event_topic = mqtt_profile['event_topic'] or 'bb-connector-default-event-topic'
            mqttc = MqttClient(mqtt_profile)
            logging.debug('Connecting to MQTT')
            mqttc.connect()

            ws_profile = app.conf['bb_server']
            logging.debug(ws_profile)
            url = f'ws://{ws_profile["hostname"]}:{ws_profile["port"]}/json'
            ws = websocket.WebSocketApp(url,
                                        on_message=on_ws_message,
                                        on_error=on_ws_error,
                                        on_close=on_ws_close)
            ws.on_open = on_ws_open
            ws.run_forever()
    except Exception as ex:
        logging.error(ex)
