FROM python:3.6-slim

LABEL Description="BB Connector"
LABEL Vendor="Avengers"
LABEL Author="Avengers"


# Setup env variables
ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  PIPENV_COLORBLIND=true \
  PIPENV_NOSPIN=true 
#PIPENV_VENV_IN_PROJECT=true 

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# # System dependencies:
# RUN apk update \
#   && apk --no-cache add \
#   bash \
#   build-base \
#   curl \
#   gcc \
#   gettext \
#   git \
#   libffi-dev \
#   linux-headers \
#   musl-dev \
#   tini

# Bundle the application, create the  app folder
# in the container and copy everything but what in the .dockerignore
WORKDIR /app
COPY . /app

# Run pipenv to download and install all the python deps
# RUN set -ex \
#   && pip install pipenv \
#   && pipenv install --system
RUN set -ex && pip install -r requirements.txt

CMD python main.py 