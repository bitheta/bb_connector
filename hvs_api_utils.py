from hvs_api_client import HVSAPIClient
from lib import app_config as app
import datetime
import random
from handwashing_event import HandwashingEvent
import logging

class HVSApiUtils:

    @staticmethod
    def post_event(handwash_event):
        hvs_client = HVSAPIClient(app.conf['hvs_api']['url'])
        response = hvs_client.login(app.conf['hvs_api']['username'], app.conf['hvs_api']['password'], app.conf['hvs_api']['domain'])
        hvs_client.get_authentication_token(response)
        # check the response code
        entity = {
            "name": "HW_Event " +  handwash_event.status_display_text,
            "tags": [],
            "address":"",
            "appearance": {},
            "description": "COVID Hand Wash Check",
            "timestamp": handwash_event.in_timestamp,
            "location": {
                "coordinates": []
            },
            "ancestors": [],
            "type_id": app.conf['hvs_api']['entity_type_id'],
            "attributes": {
                "occupied_duration_in_second": { "value": 40},
                "hand_washing_duration_threshold": { "value": handwash_event.duration_threshold},
                "hand_washing_duration_fail": { "value": handwash_event.hand_washing_duration_fail },
                "hand_washing_duration_in_second": { "value": handwash_event.hand_washing_duration },
                "hand_washing_duration_display_text": { "value": handwash_event.hand_washing_duration_display_text },
                "in_timestamp": { "value": handwash_event.in_timestamp },
                "out_timestamp": { "value": handwash_event.out_timestamp },
                "room": { "value": app.conf['mock_rooms'][random.randint(1,1)] },
                #"room": { "value": app.conf['mock_rooms'][random.randint(0,2)] },
                "soap_used": { "value": handwash_event.soap_used },
                "soap_used_display_text": { "value": handwash_event.soap_used_display_text },
                "towel_used": { "value": handwash_event.towel_used },
                "towel_used_display_text": { "value": handwash_event.towel_used_display_text },
                "status": { "value": handwash_event.status },
                "status_display_text": { "value": handwash_event.status_display_text }
            }
        }

        entity_id = hvs_client.create_entity(entity, app.conf['hvs_api']['entity_type_id'])
        return entity_id


    @staticmethod
    def post_mock_event():
        in_time = datetime.datetime.utcnow()
        #in_time = in_time.replace(day=random.randint(1, in_time.day),microsecond=0)
        out_time =in_time + datetime.timedelta(0,random.randint(20, 50))

        handwash_event = HandwashingEvent(app.conf['hand_washing_threasold_s'])
        handwash_event.in_timestamp = in_time.strftime('%m/%d/%Y %H:%M:00')
        handwash_event.out_timestamp = out_time.strftime('%m/%d/%Y %H:%M:00')
        handwash_event.set_handwashing_duration(random.randint(10,25))
        handwash_event.set_soap_used(random.randint(0,0))
        #handwash_event.set_soap_used(random.randint(0,1))
        handwash_event.set_towel_used(random.randint(0,1))
        handwash_event.set_status(1 if handwash_event.soap_used and handwash_event.towel_used and handwash_event.hand_washing_duration > app.conf['hand_washing_threasold_s'] else 0)

        entity_id = HVSApiUtils.post_event(handwash_event)
        logging.debug(entity_id)
        return entity_id