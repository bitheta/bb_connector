import datetime
import json

class HandwashingEvent():
    def __init__(self, duration_threshold = 20):
        self.duration_threshold=duration_threshold
        self.room = ''
        self.in_time = datetime.datetime.utcnow()
        self.in_timestamp = self.in_time.isoformat()
        self.out_time = None
        self.out_timestamp = ''
        self.occupied_duration_in_second = 0
        self.soap_used = 0
        self.soap_used_display_text = ""
        self.towel_used = 0
        self.towel_used_display_text = ""
        self.hand_washing_duration = 0
        self.hand_washing_duration_fail = 0
        self.hand_washing_duration_display_text = ""
        self.hand_washing_duration_threshold = duration_threshold
        self.status = 0
        self.status_display_text = ""
        self.in_hand_washing_time = None
        self.out_hand_washing_time = None


    def set_handwashing_duration(self, val):
        self.hand_washing_duration= val
        if val:
            self.hand_washing_duration_display_text= "Duration Pass"
        else:
            self.hand_washing_duration_display_text= "Duration Fail"
        self.hand_washing_duration_fail= 1 if val < self.duration_threshold else 0


    def set_soap_used(self, val):
        self.soap_used= val
        if val:
            self.soap_used_display_text= "Used Soap"
        else:
            self.soap_used_display_text= "No Soap"

    def set_towel_used(self, val):
        self.towel_used= val
        if val:
            self.towel_used_display_text= "Used Towel"
        else:
            self.towel_used_display_text= "No Towel"

    def set_status(self, val):
        self.status= val
        if val:
            self.status_display_text= "Pass"
        else:
            self.status_display_text= "Fail"


    def stop(self):
        self.out_time = datetime.datetime.utcnow()
        self.out_timestamp = self.out_time.isoformat()
        self.occupied_duration_in_second = (self.out_time - self.in_time).seconds
        if self.hand_washing_duration > self.hand_washing_duration_threshold and self.soap_used == 1:
            self.status = 1

    def start_handwashing(self):
        self.in_hand_washing_time = datetime.datetime.utcnow()

    def stop_handwashing(self):
        self.out_hand_washing_time = datetime.datetime.utcnow()
        self.hand_washing_duration = (self.out_hand_washing_time - self.in_hand_washing_time).seconds

    def to_json(self):
        return json.dumps({
            'room': self.room,
            'in_timestamp': self.in_timestamp,
            'out_timestamp': self.out_timestamp,
            'occupied_duration_in_second': self.occupied_duration_in_second,
            'soap_used': self.soap_used,
            'towel_used': self.towel_used,
            'hand_washing_duration': self.hand_washing_duration,
            'hand_washing_duration_threshold': self.hand_washing_duration_threshold,
            'status': self.status
        })
